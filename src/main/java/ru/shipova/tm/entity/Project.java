package ru.shipova.tm.entity;

import java.util.Date;

public class Project {
    private String name;
    private String id;
    private String description;
    private Date dateOfBegin;
    private Date dateOfEnd;

    public Project(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public Date getDateOfBegin() {
        return dateOfBegin;
    }

    public Date getDateOfEnd() {
        return dateOfEnd;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDateOfBegin(Date dateOfBegin) {
        this.dateOfBegin = dateOfBegin;
    }

    public void setDateOfEnd(Date dateOfEnd) {
        this.dateOfEnd = dateOfEnd;
    }
}
