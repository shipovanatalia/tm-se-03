package ru.shipova.tm;

import ru.shipova.tm.constant.CommandsConst;
import ru.shipova.tm.manager.ProjectManager;
import ru.shipova.tm.manager.TaskManager;

import java.util.Scanner;

public class App {
    public static Scanner in;

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");

        ProjectManager projectManager = new ProjectManager();
        TaskManager taskManager = new TaskManager(projectManager);
        projectManager.addTaskManager(taskManager);

        in = new Scanner(System.in);

        while (true) {
            String input = in.nextLine();

            switch (input) {
                case CommandsConst.HELP:
                    System.out.println("help: Show all commands");
                    projectManager.printAllCommands();
                    taskManager.printAllCommands();
                    System.out.println("exit: exit from application");
                    break;
                case CommandsConst.CREATE_PROJECT:
                    projectManager.create();
                    break;
                case CommandsConst.LIST_OF_PROJECTS:
                    projectManager.list();
                    break;
                case CommandsConst.CLEAR_ALL_PROJECTS:
                    projectManager.clear();
                    break;
                case CommandsConst.REMOVE_PROJECT:
                    projectManager.remove();
                    break;
                case CommandsConst.UPDATE_PROJECT:
                    projectManager.update();
                    break;
                case CommandsConst.CREATE_TASK:
                    taskManager.create();
                    break;
                case CommandsConst.LIST_OF_TASKS:
                    taskManager.list();
                    break;
                case CommandsConst.CLEAR_ALL_TASKS:
                    taskManager.clear();
                    break;
                case CommandsConst.REMOVE_TASK:
                    taskManager.remove();
                    break;
                case CommandsConst.UPDATE_TASK:
                    taskManager.update();
                    break;
                case CommandsConst.SHOW_ALL_TASKS_OF_PROJECT:
                    taskManager.showAllTasksOfProject();
                    break;
                case CommandsConst.EXIT:
                    System.exit(0);
                    break;
                default:
                    System.out.println("WRONG COMMAND. ENTER 'HELP' TO GET ALL AVAILABLE COMMANDS.");
                    break;
            }
        }
    }
}
