package ru.shipova.tm.constant;

public class CommandsConst {
    public static final String HELP = "help";
    public static final String CREATE_PROJECT = "project-create";
    public static final String CLEAR_ALL_PROJECTS = "project-clear";
    public static final String REMOVE_PROJECT = "project-remove";
    public static final String LIST_OF_PROJECTS = "project-list";
    public final static String UPDATE_PROJECT = "project-update";

    public static final String CREATE_TASK = "task-create";
    public static final String CLEAR_ALL_TASKS = "task-clear";
    public static final String REMOVE_TASK = "task-remove";
    public static final String LIST_OF_TASKS = "task-list";
    public static final String UPDATE_TASK = "task-update";
    public final static String SHOW_ALL_TASKS_OF_PROJECT = "project-show-tasks";

    public static final String EXIT = "exit";

}
