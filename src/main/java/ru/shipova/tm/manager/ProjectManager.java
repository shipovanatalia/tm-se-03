package ru.shipova.tm.manager;

import ru.shipova.tm.App;
import ru.shipova.tm.entity.Project;

import java.util.*;

public class ProjectManager {
    private Map<String, Project> projectMap;
    private List<String> listOfCommands;
    private TaskManager taskManager;

    public ProjectManager() {
        this.projectMap = new HashMap<>();
        listOfCommands = new ArrayList<>();
        listOfCommands.add("project-create: Create new project");
        listOfCommands.add("project-list: Show all projects");
        listOfCommands.add("project-clear: Remove all projects");
        listOfCommands.add("project-remove: Remove selected project");
        listOfCommands.add("project-update: update name of project");
    }

    public void addTaskManager(TaskManager taskManager){
        this.taskManager = taskManager;
    }

    public String getProjectIdByName(String projectName){
        Iterator<Map.Entry<String, Project>> iterator = projectMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Project> entry = iterator.next();
            if (projectName.equals(entry.getValue().getName())) {
                return entry.getKey();
            }
        }
        return "WRONG NAME OF PROJECT";
    }

    public void create(){
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");

        String input = App.in.nextLine();
        String id = UUID.randomUUID().toString();
        projectMap.put(id, new Project(id, input));

        System.out.println("[OK]");
    }

    public void list(){
        System.out.println("[PROJECT LIST]");

        Iterator<Map.Entry<String, Project>> iterator = projectMap.entrySet().iterator();
        int i = 1;

        while (iterator.hasNext()) {
            Map.Entry<String, Project> entry = iterator.next();
            String name = entry.getValue().getName();
            System.out.println(i + ": " + name);
            i++;
        }
    }

    public void clear(){
        projectMap.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    public void remove(){
        System.out.println("ENTER NAME:");
        String projectName = App.in.nextLine();

        taskManager.removeAllTasksOfProject(getProjectIdByName(projectName));

        Iterator<Map.Entry<String, Project>> iterator = projectMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Project> entry = iterator.next();
            if (projectName.equals(entry.getValue().getName())) {
                iterator.remove();
                System.out.println("[PROJECT " + projectName + " REMOVE]");
            }
        }
    }

    public void update(){
        System.out.println("ENTER NAME OF PROJECT TO UPDATE:");
        String oldProjectName = App.in.nextLine();

        System.out.println("ENTER NEW NAME OF PROJECT:");
        String newProjectName = App.in.nextLine();

        Iterator<Map.Entry<String, Project>> iterator = projectMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Project> entry = iterator.next();
            if (oldProjectName.equals(entry.getValue().getName())) {
                entry.getValue().setName(newProjectName);
                System.out.println("[PROJECT RENAMED]");
            }
        }
    }

    public void printAllCommands(){
        for (String command : listOfCommands) {
            System.out.println(command);
        }
    }
}
