package ru.shipova.tm.manager;

import ru.shipova.tm.App;
import ru.shipova.tm.entity.Task;

import java.util.*;

public class TaskManager {
    private Map<String, Task> taskMap;
    private List<String> listOfCommands;
    private ProjectManager projectManager;

    public TaskManager(ProjectManager projectManager) {
        this.taskMap = new HashMap<>();
        this.listOfCommands = new ArrayList<>();
        listOfCommands.add("task-create: Create new task");
        listOfCommands.add("task-list: Show all tasks");
        listOfCommands.add("task-clear: Remove all tasks");
        listOfCommands.add("task-remove: Remove selected task");
        listOfCommands.add("task-update: update name of task");
        listOfCommands.add("project-show-tasks: show all tasks of project");
        this.projectManager = projectManager;
    }

    public void create() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        String taskName = App.in.nextLine();

        System.out.println("ENTER PROJECT NAME:");
        String projectName = App.in.nextLine();

        String id = UUID.randomUUID().toString();

        taskMap.put(id, new Task(id, taskName, projectManager.getProjectIdByName(projectName)));
        System.out.println("[OK]");
    }

    public void showAllTasksOfProject() {
        System.out.println("ENTER NAME OF PROJECT:");
        String projectName = App.in.nextLine();
        String projectId = projectManager.getProjectIdByName(projectName);

        System.out.println("PROJECT " + projectName + " CONTAINS TASKS:");

        Iterator<Map.Entry<String, Task>> iterator = taskMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Task> entry = iterator.next();
            if (projectId.equals(entry.getValue().getProjectId())) {
                System.out.println("[TASK " + entry.getValue().getName() + "]");
            }
        }
    }

    public void list() {
        System.out.println("[TASK LIST]");

        Iterator<Map.Entry<String, Task>> iterator = taskMap.entrySet().iterator();
        int i = 1;

        while (iterator.hasNext()) {
            Map.Entry<String, Task> entry = iterator.next();
            String name = entry.getValue().getName();
            System.out.println(i + ": " + name);
            i++;
        }
    }

    public void clear() {
        taskMap.clear();
        System.out.println("[ALL TASKS REMOVED]");
    }

    public void remove() {
        System.out.println("ENTER NAME:");
        String input = App.in.nextLine();

        Iterator<Map.Entry<String, Task>> iterator = taskMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Task> entry = iterator.next();
            if (input.equals(entry.getValue().getName())) {
                iterator.remove();
                System.out.println("[TASK " + input + " REMOVE]");
            }
        }
    }

    public void removeAllTasksOfProject(String projectId) {
        Iterator<Map.Entry<String, Task>> iterator = taskMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Task> entry = iterator.next();
            if (projectId.equals(entry.getValue().getProjectId())) {
                iterator.remove();
                System.out.println("[TASK " + entry.getValue().getName() + " REMOVE]");
            }
        }
    }

    public void update() {
        System.out.println("ENTER NAME OF TASK TO UPDATE:");
        String oldTaskName = App.in.nextLine();

        System.out.println("ENTER NEW NAME OF TASK:");
        String newTaskName = App.in.nextLine();

        Iterator<Map.Entry<String, Task>> iterator = taskMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Task> entry = iterator.next();
            if (oldTaskName.equals(entry.getValue().getName())) {
                entry.getValue().setName(newTaskName);
                System.out.println("[TASK RENAMED]");
            }
        }
    }

    public void printAllCommands() {
        for (String command : listOfCommands) {
            System.out.println(command);
        }
    }
}
